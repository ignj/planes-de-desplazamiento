/*
   1 2 3 4 5 6 7 8 9 10
1  # # # # # # # # # #
2  # - ^ ^ ^ ^ ^ ^ ^ #
3  # - ^ ^ ^ ^ ^ ^ ^ #
4  # - ^ ^ ^ ^ ^ ^ ^ #
5  # - ^ ^ ^ ^ ^ ^ ^ #
6  # - ^ ^ ^ ^ ^ ^ ^ #
7  # - ^ ^ ^ ^ ^ ^ ^ #
8  # - ^ ^ ^ ^ ^ ^ ^ W
9  # - ^ ^ ^ ^ ^ ^ ^ #
10 # - ^ ^ ^ ^ ^ ^ ^ #
11 # - ^ ^ ^ ^ ^ ^ - #
12 # - ^ ^ ^ ^ ^ ^ - #
13 # - ^ ^ ^ ^ ^ ^ - #
14 # - - - - - - - - #
15 # # # # # # W # # #
*/

land([1,1], forest).
land([1,2], forest).
land([1,3], forest).
land([1,4], forest).
land([1,5], forest).
land([1,6], forest).
land([1,7], forest).
land([1,8], forest).
land([1,9], forest).
land([1,10], forest).

land([2,1], forest).
land([2,2], plain).
land([2,3], mountain).
land([2,4], mountain).
land([2,5], mountain).
land([2,6], mountain).
land([2,7], mountain).
land([2,8], mountain).
land([2,9], mountain).
land([2,10], forest).

land([3,1], forest).
land([3,2], plain).
land([3,3], mountain).
land([3,4], mountain).
land([3,5], mountain).
land([3,6], mountain).
land([3,7], mountain).
land([3,8], mountain).
land([3,9], mountain).
land([3,10], forest).

land([4,1], forest).
land([4,2], plain).
land([4,3], mountain).
land([4,4], mountain).
land([4,5], mountain).
land([4,6], mountain).
land([4,7], mountain).
land([4,8], mountain).
land([4,9], mountain).
land([4,10], forest).

land([5,1], forest).
land([5,2], plain).
land([5,3], mountain).
land([5,4], mountain).
land([5,5], mountain).
land([5,6], mountain).
land([5,7], mountain).
land([5,8], mountain).
land([5,9], mountain).
land([5,10], forest).

land([6,1], forest).
land([6,2], plain).
land([6,3], mountain).
land([6,4], mountain).
land([6,5], mountain).
land([6,6], mountain).
land([6,7], mountain).
land([6,8], mountain).
land([6,9], mountain).
land([6,10], forest).

land([7,1], forest).
land([7,2], plain).
land([7,3], mountain).
land([7,4], mountain).
land([7,5], mountain).
land([7,6], mountain).
land([7,7], mountain).
land([7,8], mountain).
land([7,9], mountain).
land([7,10], forest).

land([8,1], forest).
land([8,2], plain).
land([8,3], mountain).
land([8,4], mountain).
land([8,5], mountain).
land([8,6], mountain).
land([8,7], mountain).
land([8,8], mountain).
land([8,9], mountain).
land([8,10], forest).

land([9,1], forest).
land([9,2], plain).
land([9,3], mountain).
land([9,4], mountain).
land([9,5], mountain).
land([9,6], mountain).
land([9,7], mountain).
land([9,8], mountain).
land([9,9], mountain).
land([9,10], forest).

land([10,1], forest).
land([10,2], plain).
land([10,3], mountain).
land([10,4], mountain).
land([10,5], mountain).
land([10,6], mountain).
land([10,7], mountain).
land([10,8], mountain).
land([10,9], mountain).
land([10,10], forest).

land([11,1], forest).
land([11,2], plain).
land([11,3], mountain).
land([11,4], mountain).
land([11,5], mountain).
land([11,6], mountain).
land([11,7], mountain).
land([11,8], mountain).
land([11,9], plain).
land([11,10], forest).

land([12,1], forest).
land([12,2], plain).
land([12,3], mountain).
land([12,4], mountain).
land([12,5], mountain).
land([12,6], mountain).
land([12,7], mountain).
land([12,8], mountain).
land([12,9], plain).
land([12,10], forest).

land([13,1], forest).
land([13,2], plain).
land([13,3], mountain).
land([13,4], mountain).
land([13,5], mountain).
land([13,6], mountain).
land([13,7], mountain).
land([13,8], mountain).
land([13,9], plain).
land([13,10], forest).

land([14,1], forest).
land([14,2], plain).
land([14,3], plain).
land([14,4], plain).
land([14,5], plain).
land([14,6], plain).
land([14,7], plain).
land([14,8], plain).
land([14,9], plain).
land([14,10], forest).

land([15,1], forest).
land([15,2], forest).
land([15,3], forest).
land([15,4], forest).
land([15,5], forest).
land([15,6], forest).
land([15,7], forest).
land([15,8], forest).
land([15,9], forest).
land([15,10], forest).

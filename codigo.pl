% Configuraci�n de la comarca
/*
   1 2 3 4 5 6 7 8 9 10
1  # # # # # # # # # #
2  # - - ^ - - - # h #
3  # - # - - ^ - # - #
4  # - # # # # - - ^ #
5  # - # - - # - - - #
6  # - - - - # - - ^ #
7  # - - # # # - ^ ^ #
8  # g - # - - - ^ - W
9  # - - - - - ^ g - #
10 # - - W - - - - - #
11 # - W W W W - - - #
12 # - - # h W W - # #
13 # - - - - - W - # #
14 # - - - - ^ ^ ^ - #
15 # # # # # # W # # #

Configuraci�n para el suelo

land(Pos, Land)
Land: plain (-)
      water (W)
      mountain (^)
      forest (#)

at(Pos, Building)
Building: hostel (h)
          grave (g)

*/

% Creaci�n del mapa a trav�s de hechos.
land([1,1], forest).
land([1,2], forest).
land([1,3], forest).
land([1,4], forest).
land([1,5], forest).
land([1,6], forest).
land([1,7], forest).
land([1,8], forest).
land([1,9], forest).
land([1,10], forest).

land([2,1], forest).
land([2,2], plain).
land([2,3], plain).
land([2,4], mountain).
land([2,5], plain).
land([2,6], plain).
land([2,7], plain).
land([2,8], forest).
land([2,9], plain).
land([2,10], forest).

land([3,1], forest).
land([3,2], plain).
land([3,3], forest).
land([3,4], plain).
land([3,5], plain).
land([3,6], mountain).
land([3,7], plain).
land([3,8], forest).
land([3,9], plain).
land([3,10], forest).

land([4,1], forest).
land([4,2], plain).
land([4,3], forest).
land([4,4], forest).
land([4,5], forest).
land([4,6], forest).
land([4,7], plain).
land([4,8], plain).
land([4,9], mountain).
land([4,10], forest).

land([5,1], forest).
land([5,2], plain).
land([5,3], forest).
land([5,4], plain).
land([5,5], plain).
land([5,6], forest).
land([5,7], plain).
land([5,8], plain).
land([5,9], plain).
land([5,10], forest).

land([6,1], forest).
land([6,2], plain).
land([6,3], plain).
land([6,4], plain).
land([6,5], plain).
land([6,6], forest).
land([6,7], plain).
land([6,8], plain).
land([6,9], mountain).
land([6,10], forest).

land([7,1], forest).
land([7,2], plain).
land([7,3], plain).
land([7,4], forest).
land([7,5], forest).
land([7,6], forest).
land([7,7], plain).
land([7,8], mountain).
land([7,9], mountain).
land([7,10], forest).

land([8,1], forest).
land([8,2], plain).
land([8,3], plain).
land([8,4], forest).
land([8,5], plain).
land([8,6], plain).
land([8,7], plain).
land([8,8], mountain).
land([8,9], plain).
land([8,10], water).

land([9,1], forest).
land([9,2], plain).
land([9,3], plain).
land([9,4], plain).
land([9,5], plain).
land([9,6], plain).
land([9,7], mountain).
land([9,8], plain).
land([9,9], plain).
land([9,10], forest).

land([10,1], forest).
land([10,2], plain).
land([10,3], plain).
land([10,4], water).
land([10,5], plain).
land([10,6], plain).
land([10,7], plain).
land([10,8], plain).
land([10,9], plain).
land([10,10], forest).

land([11,1], water).
land([11,2], plain).
land([11,3], water).
land([11,4], water).
land([11,5], water).
land([11,6], water).
land([11,7], plain).
land([11,8], plain).
land([11,9], plain).
land([11,10], forest).

land([12,1], forest).
land([12,2], plain).
land([12,3], plain).
land([12,4], forest).
land([12,5], plain).
land([12,6], water).
land([12,7], water).
land([12,8], plain).
land([12,9], forest).
land([12,10], forest).

land([13,1], forest).
land([13,2], plain).
land([13,3], plain).
land([13,4], plain).
land([13,5], plain).
land([13,6], plain).
land([13,7], water).
land([13,8], plain).
land([13,9], forest).
land([13,10], forest).

land([14,1], forest).
land([14,2], plain).
land([14,3], plain).
land([14,4], plain).
land([14,5], plain).
land([14,6], mountain).
land([14,7], mountain).
land([14,8], mountain).
land([14,9], plain).
land([14,10], forest).

land([15,1], forest).
land([15,2], forest).
land([15,3], forest).
land([15,4], forest).
land([15,5], forest).
land([15,6], forest).
land([15,7], water).
land([15,8], forest).
land([15,9], forest).
land([15,10], forest).

at([2,9], hostel).
at([8,2], grave).
at([9,8], grave).
at([12,5], hostel).

/* Acciones de desplazamiento */

/*
turn(+Dir) donde Dir pertenece a {n,s,e,w}.
Gira al agente hacia una posici�n determinada.
*/
turn(n).
turn(s).
turn(e).
turn(w).

/*
move_fwd.
Desplaza al explorador una posici�n en la direcci�n que se encuentra mirando.
*/
move_fwd.

/*
Implementaci�n de heuristica: Distancia de Manhattan
heuristica(+PosicionOrigen, +Metas, -Costo).
A partir de la posicion de origen y de un conjunto de metas, se retorna el menor costo
encontrado entre el origen y una meta.
*/
heuristica(PosicionOrigen, Metas, Costo):-
                     PosicionOrigen = [XOrigen, YOrigen],
                     findall(CostoMetaX,
                             (member(MetaX, Metas),
                              MetaX = [XDestino, YDestino],
                              CostoMetaX is abs(XOrigen - XDestino) + abs(YOrigen - YDestino)
                              ),
                             ListaDeCostos),
                     min_list(ListaDeCostos, Costo).

/*
es_meta(+Nodo, +Metas).
Es verdadero si la Posici�n del Nodo esta contenida dentro de la lista Metas.
*/
es_meta(Nodo, Metas):- Nodo = nodo(Estado, _CaminoActual, _CostoActual, _ValorF),
                       Estado = estado(Pos, _Direccion),
                       member(Pos, Metas).

/*
seleccionar(-Nodo, +Frontera, -FronteraReducida).
Obtiene el primer nodo de la frontera. Retorna el nodo obtenido y la frontera resultante de quitar
el nodo seleccionado.
*/
seleccionar(Nodo, [Nodo | FronteraReducida], FronteraReducida).

/*
buscar_vecinos(+Nodo, +Frontera, +Visitados, +Metas, -Vecinos, -VisitadosActualizados, -FronteraActualizada).
A partir de Nodo se obtienen sus Vecinos, estos surgen de la aplicacion de los operadores disponibles.
Se obtiene tambien a las listas de VecinosActualizados y FronteraActualizada en base a los Vecinos encontrados
*/
buscar_vecinos(Nodo, Frontera, Visitados, Metas, Vecinos, VisitadosActualizados, FronteraActualizada):-
                 Nodo = nodo(EstadoActual, CaminoActual, CostoActual, _ValorF),
                 findall(nodo(estado(PosSiguiente, DirSiguiente),
                              CaminoResultante,
                              CostoSiguiente,
                              ValorFSiguiente
                             ),
                         (siguiente_nodo(EstadoActual, estado(PosSiguiente, DirSiguiente), Accion, CostoAccion),
                          CostoSiguiente is CostoActual + CostoAccion,
                          heuristica(PosSiguiente, Metas, CostoHeuristica),
                          ValorFSiguiente is CostoSiguiente + CostoHeuristica,
                          append(Accion, CaminoActual, CaminoResultante)
                         ),
                         VecinosSinFiltrar),
                 filtrar_vecinos(VecinosSinFiltrar, Frontera, Visitados, Vecinos, VisitadosActualizados, FronteraActualizada).
                 
/*
filtrar_vecinos(+VecinosSinFiltrar, +Frontera, +Visitados, -Vecinos, -VisitadosActualizados, -FronteraActualizadaPorVecinos).
A partir de la lista VecinosSinFiltrar se actualiza a la Frontera y a los Visitados, buscando detectar mejores caminos para llegar a un estado.
A partir de estas actualizaciones surgen las listas VisitadosActualizados y FronteraActualizadaPorVecinos.
Finalmente se obtiene la lista Vecinos a partir de aquellos VecinosSinFiltrar que no estan en la frontera actualizada ni en los visitados actualizados.
*/
filtrar_vecinos(VecinosSinFiltrar, Frontera, Visitados, Vecinos, VisitadosActualizados, FronteraActualizadaPorVecinos):-
                 actualizar_frontera(Frontera, VecinosSinFiltrar, FronteraActualizada),
                 actualizar_visitados(FronteraActualizada, VecinosSinFiltrar, Visitados, VisitadosActualizados, FronteraActualizadaPorVecinos),
                 findall(Nodo,
                         (
                          Nodo = nodo(Estado, _Camino, _Costo, _ValorF),
                          member(Nodo, VecinosSinFiltrar),
                          not(member(nodo(Estado, _, _, _), FronteraActualizadaPorVecinos)),
                          not(member(nodo(Estado, _), VisitadosActualizados))
                         ),
                         Vecinos
                 ).
/*
actualizar_visitado(+FronteraActualizada, +VecinosSinFiltrar, +Visitados, -VisitadosActualizados, -FronteraActualizadaPorVecinos)
Si se encuentran mejores caminos en VecinosSinFiltrar para nodos que ya han sido Visitados, se agrega a estos nodos
en la frontera (permitiendo la exploracion de nuevos caminos mas eficientes) y se los quita de la lista de nodos visitados.
*/
actualizar_visitados(FronteraActualizada, VecinosSinFiltrar, Visitados, VisitadosActualizados, FronteraActualizadaPorVecinos):-
                 findall(Nodo,
                         (
                          Nodo = nodo(Estado, Camino, Costo, FVecinoNuevo),
                          member(nodo(Estado, Camino, Costo, FVecinoNuevo), VecinosSinFiltrar),
                          member(nodo(Estado, FVisitado), Visitados),
                          FVecinoNuevo < FVisitado
                         ),
                         VisitadosAReincorporar
                 ),
                 findall(Nodo,
                         (
                          Nodo = nodo(Estado, ValorF),
                          member(nodo(Estado, ValorF), Visitados),
                          \+(member(nodo(Estado, _, _, _), VisitadosAReincorporar))
                         ),
                         VisitadosActualizados
                 ),
                 union(FronteraActualizada, VisitadosAReincorporar, FronteraActualizadaPorVecinos).

/*
actualizar_frontera(+Frontera, +VecinosSinFiltrar, -FronteraActualizada)
A partir de los VecinosSinFiltrar, actualiza la frontera en el caso de encontrar mejores caminos
para continuar con la b�squeda.
*/
actualizar_frontera(Frontera, VecinosSinFiltrar, FronteraActualizada):-
                 findall(Nodo,
                         (
                          Nodo = nodo(Estado, Camino, Costo, FVecinos),
                          member(nodo(Estado, _, _, FFrontera), Frontera),
                          member(nodo(Estado, Camino, Costo, FVecinos), VecinosSinFiltrar),
                          FVecinos < FFrontera
                         ),
                         NodosParaActualizarEnFrontera
                 ),
                 findall(Nodo,
                         (
                          Nodo = nodo(Estado, Camino, Costo, ValorF),
                          member(nodo(Estado, Camino, Costo, ValorF), Frontera),
                          \+(member(nodo(Estado, _, _, _), NodosParaActualizarEnFrontera))
                         ),
                         NodosSinCambiosEnFrontera
                 ),
                 union(NodosParaActualizarEnFrontera, NodosSinCambiosEnFrontera, FronteraActualizada).
                 
/*
siguiente_nodo(+EstadoActual, -EstadoSiguiente, -AccionRealizada, -CostoDeMovimiento)
Se refiere a la aplicacion de un operador asociado a los movimientos disponibles.
Dependiendo del operador aplicado (giro o moverse hacia adelante), se obtiene el EstadoSiguiente, una lista
que contiene a la AccionRealizada, y el CostoDeMovimiento asociado al operador aplicado.
*/
/* Accion de giro */
siguiente_nodo(estado(PosicionActual, DireccionActual), estado(PosicionActual, DireccionSiguiente), [turn(DireccionSiguiente)], CostoDeMovimiento):-
                 turn(DireccionSiguiente),
                 dif(DireccionActual, DireccionSiguiente),
                 obtener_costo_de_giro(DireccionActual, DireccionSiguiente, CostoDeMovimiento).

/* Movimiento hacia adelante */
siguiente_nodo(estado(PosicionActual, DireccionActual), estado(PosicionSiguiente, DireccionActual), [move_fwd], CostoDeMovimiento):-
                 obtener_posicion_siguiente(PosicionActual, DireccionActual, PosicionSiguiente),
                 es_transitable(PosicionSiguiente),
                 costo_de_desplazamiento(PosicionSiguiente, CostoDeMovimiento),
                 move_fwd.

/*
obtener_posicion_siguiente(+PosicionActual, +Direccion, -PosicionDestino).
A partir de la PosicionActual y la Direccion, se obtienen las coordenadas de la PosicionDestino.
*/

%Recordar que las filas (horizontales X) comienzan en 1 en el norte y van incrementando su valor hacia el sur.
obtener_posicion_siguiente([XActual,YActual], n, [XSiguiente, YActual]):- XSiguiente is XActual - 1.
obtener_posicion_siguiente([XActual,YActual], s, [XSiguiente, YActual]):- XSiguiente is XActual + 1.
%Recordar que las columnas (verticales Y) comienzan en 1 en el oeste y van incrementando su valor hacia el este.
obtener_posicion_siguiente([XActual,YActual], w, [XActual, YSiguiente]):- YSiguiente is YActual - 1.
obtener_posicion_siguiente([XActual,YActual], e, [XActual, YSiguiente]):- YSiguiente is YActual + 1.

/*
es_transitable(+Posicion).
Es verdadero si la Posici�n es transitable y no tiene ninguna edificaci�n.
*/
es_transitable(Posicion):- land(Posicion, Z), member(Z, [plain, mountain]), not(at(Posicion, _Building)).

/*
costo_de_desplazamiento(+Posicion, -Costo).
Se obtiene el Costo de desplazarse a la Posicion especificada.
*/
costo_de_desplazamiento(Posicion, 1):- land(Posicion, plain).
costo_de_desplazamiento(Posicion, 3):- land(Posicion, mountain).
                 
/*
obtener_costo_de_giro(+DireccionInicial, +DireccionFinal, -Costo).
Determina el costo de girar desde la direccion inicial hasta la direccion final.
*/
obtener_costo_de_giro(X, Y, 1):- member(X, [n,s]), member(Y, [e,w]).
obtener_costo_de_giro(X, Y, 1):- member(X, [e,w]), member(Y, [n,s]).
obtener_costo_de_giro(n, s, 2).
obtener_costo_de_giro(s, n, 2).
obtener_costo_de_giro(e, w, 2).
obtener_costo_de_giro(w, e, 2).
                 
/*
agregar_vecinos(+FronteraReducida, +Vecinos, -NuevaFrontera).
Se obtiene la lista NuevaFrontera que contiene a los nodos de FronteraReducida y
Vecinos ordenados segun la funcion f(n).
*/
agregar_vecinos(FronteraReducida, Vecinos, NuevaFrontera):-
                append(FronteraReducida, Vecinos, FronteraDesordenada),
                sort(4, @=<, FronteraDesordenada, NuevaFrontera).

/*
buscar_plan_desplazamiento(+EstadoInicial, +Metas, -Destino, -Plan, -Costo)
Dado un estado inicial y un conjunto de posiciones meta, encuentra la posici�n
meta (Destino) a la que le cueste menos llegar. El Plan es una secuencia de acciones de desplazamiento
(move_fwd y turn(Dir)) que le permiten llegar al destino. El Costo es el costo total de ejecutar
el Plan definido.
Esta funcion sirve de cascaron para el predicado buscar_a_estrella(+Frontera, +Metas, +Visitados, +Destino, +Plan, +Costo).

Ejecutar como:
         buscar_plan_desplazamiento(estado([2,2], n), [[2,2]], Destino, Plan, Costo).
Ver descripcion del nodo en el informe.
*/

buscar_plan_desplazamiento(EstadoInicial, Metas, Destino, Plan, Costo):-
                 buscar_a_estrella([nodo(EstadoInicial, [], 0, 0)], Metas, [], Destino, Plan, Costo),
                 !.
                 
buscar_plan_desplazamiento(_EstadoInicial, _Metas, _Destino, _Plan, _Costo):-
                 writeln('No se ha encontrado ning�n camino para las metas especificadas'), fail.
                 
buscar_a_estrella(Frontera, Metas, _Visitados, Destino, Plan, Costo):-
                 seleccionar(Nodo, Frontera, _FronteraReducida),
                 es_meta(Nodo, Metas),
                 Nodo = nodo(Estado, PlanSinInvertir, CostoReal, _ValorF),
                 Estado = estado(Posicion, _Direccion),
                 reverse(PlanSinInvertir, Plan),
                 Costo = CostoReal,
                 Destino = Posicion,
                 !.
                 
buscar_a_estrella(Frontera, Metas, Visitados, Destino, Plan, Costo):-
                 seleccionar(Nodo, Frontera, FronteraReducida),
                 buscar_vecinos(Nodo, FronteraReducida, Visitados, Metas, Vecinos, VisitadosActualizados, FronteraActualizada),
                 agregar_vecinos(FronteraActualizada, Vecinos, NuevaFrontera),
                 Nodo = nodo(Estado, _CaminoActual, _CostoActual, ValorF),
                 %writeln(Estado),
                 buscar_a_estrella(NuevaFrontera, Metas, [nodo(Estado, ValorF) | VisitadosActualizados], Destino, Plan, Costo).
